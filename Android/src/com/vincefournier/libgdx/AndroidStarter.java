package com.vincefournier.libgdx;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.vincefournier.game.LibgdxGame;

/**
 * Entrance point for the Android Application.
 */
public class AndroidStarter extends AndroidApplication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidApplicationConfiguration configuration = new AndroidApplicationConfiguration();
        configuration.useAccelerometer = false;
        configuration.useCompass = false;
        configuration.useWakelock = true;
        configuration.useGL20 = true;

        initialize(new LibgdxGame(), configuration);
    }
}
