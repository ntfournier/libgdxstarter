package com.vincefournier.libgdx;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.vincefournier.game.LibgdxGame;

/**
 * This class is the entrance point for the desktop application.
 */
public class DesktopStarter {
    public static final String APPLICATION_NAME = "Libgdx Starter";
    public static final int WIDTH = 800;
    public static final int HEIGHT = 480;

    public static void main(String[] args) {
        LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();

        configuration.title = APPLICATION_NAME;
        configuration.useGL20 = false;
        configuration.width = WIDTH;
        configuration.height = HEIGHT;

        // Disabling vSync for debug can be useful to show maximum frame when doing benchmark on your application.
        configuration.vSyncEnabled = true;

        new LwjglApplication(new LibgdxGame(), configuration);
    }
}
