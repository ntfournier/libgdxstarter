package com.vincefournier.game;

import com.badlogic.gdx.Game;
import com.vincefournier.game.screen.GameScreen;

/**
 * Entrance point for the Libgdx game.
 */
public class LibgdxGame extends Game {

    /**
     * Create the first shown screen.
     */
    @Override public void create() {
        setScreen(new GameScreen(this, "default"));
    }

    @Override public void dispose() {
        super.dispose();
        //getScreen().dispose();
    }
}
