package com.vincefournier.game.screen;

import com.badlogic.gdx.Game;
import com.vincefournier.frameworklib.userinterface.AbstractGameScreen;
import com.vincefournier.game.Simulation.ConcreteRenderer;
import com.vincefournier.game.Simulation.ConcreteSimulation;

/**
 * GameScreen, class useful to create simulation screen.
 * This is the screen containing the simulation.
 */
public class GameScreen extends AbstractGameScreen {
    /**
     * Default constructor for the concrete game screen.
     * @param game The game
     * @param levelName The level that need to be initialize.
     */
    public GameScreen(Game game, String levelName) {
        super(game, levelName);
    }

    @Override protected void initializeConcreteSimulation(String gameLevel) {
        this.simulation = new ConcreteSimulation(level);
        this.simulationRenderer = new ConcreteRenderer(spriteBatch, simulation);
    }

    @Override public void loadAssets() {
        // Load button textures here...
    }

    @Override public void initializeButtons() {

    }

    @Override public void updateButtons(String buttonName) {

    }

    @Override public void updateIsTouch(float inputXPosition, float inputYPosition) {

    }

    @Override public void draw() {
        simulationRenderer.draw();
    }
}
