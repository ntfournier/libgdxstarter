package com.vincefournier.game.Simulation;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.vincefournier.frameworklib.simulation.Simulation;
import com.vincefournier.frameworklib.simulation.SimulationRenderer;

/**
 * ConcreteRenderer,
 * Created by Vince Fournier on 19/12/13.
 */
public class ConcreteRenderer extends SimulationRenderer {
    // All the textures and animations name.
    private static final String SOLDIER = "soldier";
    private static final String ARCHER = "archer";
    private static final String HOUSE = "house";
    private static final String BACKGROUND = "background";

    public ConcreteRenderer(SpriteBatch spriteBatch, Simulation simulation) {
        super(spriteBatch, simulation);
    }

    @Override
    public void loadTextures() {
        TextureAtlas atlas = new TextureAtlas("gameScreen.txt");

        // Todo change for loadTextures(String name...);
        textureRegionHashMap.put(SOLDIER, atlas.findRegion(SOLDIER));
        textureRegionHashMap.put(ARCHER, new TextureRegion(atlas.findRegion(ARCHER)));
        textureRegionHashMap.put(HOUSE, new TextureRegion(atlas.findRegion(HOUSE)));
        textureRegionHashMap.put(BACKGROUND, new TextureRegion(atlas.findRegion(BACKGROUND)));
    }

    @Override
    public void draw() {
        super.draw();

        // Todo change to reduce call length
        OrthographicCamera camera = simulation.getCurrentCamera();
        spriteBatch.draw(textureRegionHashMap.get(BACKGROUND),
                camera.position.x - camera.viewportWidth / 2, camera.position.y
                - camera.viewportHeight / 2, camera.viewportWidth, camera.viewportHeight);
    }
}
