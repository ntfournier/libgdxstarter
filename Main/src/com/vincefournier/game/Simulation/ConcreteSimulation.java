package com.vincefournier.game.Simulation;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.vincefournier.frameworklib.simulation.Simulation;

/**
 * ConcreteSimulation,
 * Created by Vince Fournier on 11/12/13.
 */
public class ConcreteSimulation extends Simulation {


    public ConcreteSimulation(String level) {
        super(level);
    }

    @Override protected void initializeCameras() {
        currentCamera = new OrthographicCamera(200, 200);
    }

    @Override public void initializeLevel() {

    }

    @Override public void update(float delta) {

    }
}
