package com.vincefournier.frameworklib.simulation;

import com.badlogic.gdx.graphics.OrthographicCamera;

import java.util.HashMap;

/**
 * Simulation, This manage all the game objects.
 * Created by Vince Fournier on 11/12/13.
 */
public abstract class Simulation {
    // Cameras of the simulation.
    protected OrthographicCamera currentCamera;
    protected HashMap<String, OrthographicCamera> cameras;

    public Simulation(String level) {
        initializeLevel();
        initializeCameras();
    }

    protected abstract void initializeCameras();
    protected abstract void initializeLevel();

    public OrthographicCamera getCurrentCamera() {
        return currentCamera;
    }


    public abstract void update(float delta);
}
