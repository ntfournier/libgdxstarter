package com.vincefournier.frameworklib.simulation;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

/**
 * SimulationRenderer,
 * Created by Vince Fournier on 12/12/13.
 */
public abstract class SimulationRenderer {
    protected SpriteBatch spriteBatch;
    protected Simulation simulation;

    protected HashMap<String, TextureRegion> textureRegionHashMap;
    protected HashMap<String, Animation> animationHashMap;

    /**
     * Default constructor assign the sprite batch and the simulation.
     * @param spriteBatch The sprite batch use for draw.
     * @param simulation The simulation we will draw.
     */
    public SimulationRenderer(SpriteBatch spriteBatch, Simulation simulation) {
        this.spriteBatch = spriteBatch;
        this.simulation = simulation;

        textureRegionHashMap = new HashMap<String, TextureRegion>();
        animationHashMap = new HashMap<String, Animation>();

        loadTextures();
    }

    public abstract void loadTextures();

    public void draw() {
        simulation.currentCamera.update();
        spriteBatch.setProjectionMatrix(simulation.currentCamera.combined);
    }


}
