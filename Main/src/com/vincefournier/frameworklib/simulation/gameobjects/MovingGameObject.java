package com.vincefournier.frameworklib.simulation.gameobjects;

import com.badlogic.gdx.math.Vector2;

/**
 * MovingGameObject,
 * Created by Vince Fournier on 12/12/13.
 */
public class MovingGameObject extends AbstractGameObject {
    public Vector2 speed;

    /**
     * GameObjects constructor with base space parameters.
     *
     * @param xPosition MovingGameObject initial x position.
     * @param yPosition MovingGameObject initial y position.
     * @param width     MovingGameObject initial width.
     * @param height    MovingGameObject initial height.
     * @param speed     MovingGameObject initial speed.
     */
    public MovingGameObject(float xPosition, float yPosition, float width, float height,
                            Vector2 speed) {
        super(xPosition, yPosition, width, height);

        this.speed = speed;
    }

    @Override
    public void update(float delta) {

    }
}
