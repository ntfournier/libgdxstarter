package com.vincefournier.frameworklib.simulation.gameobjects;

/**
 * IGameObject, Base class for all object in the game.
 * Created by Vince Fournier on 11/12/13.
 */
public abstract class AbstractGameObject {
    public float xPosition; // These properties are public to simplify access.
    public float yPosition;
    public float width;
    public float height;

    /**
     * GameObjects constructor with base space parameters.
     * @param xPosition SimpleGameObject x position.
     * @param yPosition SimpleGameObject y position.
     * @param width SimpleGameObject width.
     * @param height SimpleGameObject height.
     */
    public AbstractGameObject(float xPosition, float yPosition, float width, float height) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.width = width;
        this.height = height;
    }

    public abstract void update(float delta);
}
