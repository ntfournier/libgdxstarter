package com.vincefournier.frameworklib.engine;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Classe contenant des fonctions servant a verifier si un point fait partie d'un rectangle ou si deux rectangle se
 * croisent.
 * 
 * @author Vincent Fournier
 */
public class UtilitaireMath {
	public static boolean rectanglesSuperpose(Rectangle r1, Rectangle r2) {
		return (r1.x < r2.x + r2.width && r1.x + r1.width > r2.x && r1.y < r2.y + r2.height && r1.y + r1.height > r2.y);
	}

	public static boolean pointDansRectangle(Rectangle r, Vector2 p) {
		return r.x <= p.x && r.x + r.width >= p.x && r.y <= p.y && r.y + r.height >= p.y;
	}

	public static boolean pointDansRectangle(Rectangle r, float x, float y) {
		return r.x <= x && r.x + r.width >= x && r.y <= y && r.y + r.height >= y;
	}

	public static boolean pointDansRectangleIncline(float p_x, float p_y, float p_coordMilieuX, float p_coordMilieuY,
			float p_largeurRectangle, float p_hauteurRectangle, double p_angleRadian) {
		double distanceX = p_x - p_coordMilieuX;
		double distanceY = p_y - p_coordMilieuY;
		double distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

		double angleCourrant = Math.atan2(distanceY, distanceX);
		double angleNouveau = angleCourrant - p_angleRadian;

		double x2 = Math.cos(angleNouveau) * distance;
		double y2 = Math.sin(angleNouveau) * distance;
		/*
		 * System.out.println("*** Test fonction : pointDansRectangleIncline ***\n" + "Params : p_x " + p_x + " p_y " +
		 * p_y + "\nCoord Milieu : (" + p_coordMilieuX + ", " + p_coordMilieuY + ")\n" + "Largeur : " +
		 * p_largeurRectangle + " Hauteur : " + p_hauteurRectangle + "\n" + "Angle en degrees : " +
		 * Math.toDegrees(p_angleRadian) + "\n");
		 */
		return (x2 > -.5 * p_largeurRectangle && x2 < .5 * p_largeurRectangle
                && y2 > -.5 * p_hauteurRectangle && y2 < .5 * p_hauteurRectangle);
	}

	public static double pythagore(float p_a, float p_b) {
		return Math.sqrt(Math.pow(p_a, 2) + Math.pow(p_b, 2));
	}

	public static double distanceBetweenPoints(Vector2 p_pointA, Vector2 p_pointB) {
		return pythagore(p_pointA.x - p_pointB.x, p_pointA.y - p_pointB.y);
	}

	public static float obtenirAngleRadianSelonPoints(Vector2 p_pointInitial, Vector2 p_pointFinal) {
		return (float) (Math.atan((p_pointFinal.y - p_pointInitial.y) / (p_pointFinal.x - p_pointInitial.x)) + Math.PI / 2);
	}

	public static Vector2 obtenirPointMilieuEntrePoints(Vector2 p_pointA, Vector2 p_pointB) {
		float x;
		if (p_pointA.x < p_pointB.x) {
			x = p_pointB.x + (p_pointA.x - p_pointB.x) / 2;
		} else {
			x = p_pointA.x - (p_pointA.x - p_pointB.x) / 2;
		}

		float y;
		if (p_pointA.y < p_pointB.y) {
			y = p_pointB.y + (p_pointA.y - p_pointB.y) / 2;
		} else {
			y = p_pointA.y - (p_pointA.y - p_pointB.y) / 2;
		}

		return new Vector2(x, y);
	}
}