package com.vincefournier.frameworklib.userinterface;

import com.badlogic.gdx.Game;
import com.vincefournier.frameworklib.simulation.Simulation;
import com.vincefournier.frameworklib.simulation.SimulationRenderer;

/**
 * AbstractGameScreen,
 * Created by Vince Fournier on 12/12/13.
 */
public abstract class AbstractGameScreen extends AbstractScreen {
    protected String level;

    protected Simulation simulation;
    protected SimulationRenderer simulationRenderer;

    public AbstractGameScreen(Game game, String gameLevel) {
        super(game);

        this.level = gameLevel;
        initializeConcreteSimulation(gameLevel);
    }

    protected abstract void initializeConcreteSimulation(String gameLevel);

    @Override public final void update(float delta) {
        simulation.update(delta);
    }

    /**
     * Background isn't draw in game screen.
     *
     * @return if the background is draw.
     */
    @Override protected boolean isBackgroundDraw() {
        return false;
    }
}
