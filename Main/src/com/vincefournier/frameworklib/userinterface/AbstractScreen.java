package com.vincefournier.frameworklib.userinterface;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.vincefournier.frameworklib.userinterface.uiobjects.Button;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * AbstractScreen for libgdx application.
 */
public abstract class AbstractScreen implements Screen {
    public static final String TEXTURE_BACKGROUND = "background";
    public static final int VIEW_WIDTH = 800;   // Should be the same ratio of the application.
    public static final int VIEW_HEIGHT = 480;

    private Game game; // The game instance.

    protected OrthographicCamera guiCamera;

    private Vector3 touchPosition;

    private float viewWidth;
    private float viewHeight;

    protected SpriteBatch spriteBatch;

    protected HashMap<String, TextureRegion> textures; // Use to store textures.
    protected HashMap<String, Animation> animations;   // Use to store animations.
    protected HashMap<String, Button> buttons;         // Use to store buttons.

    public AbstractScreen(Game game) {
        this.game = game;

        viewWidth = VIEW_WIDTH;
        viewHeight = VIEW_HEIGHT;

        guiCamera = new OrthographicCamera(viewWidth, viewHeight);
        guiCamera.position.set(viewWidth / 2, viewHeight / 2, 0);
        touchPosition = new Vector3();

        spriteBatch = new SpriteBatch();

        // Initialize the Hash map.
        textures = new HashMap<String, TextureRegion>();
        animations = new HashMap<String, Animation>();
        buttons = new HashMap<String, Button>();

        loadAssets();
        initializeButtons();
    }

    /**
     * Load all the assets (animations and texture).
     */
    public abstract void loadAssets();

    /**
     * Initialize all the buttons.
     */
    public abstract void initializeButtons();

    /**
     * Method responsible for the main loop update.
     *
     * @param delta Time pass since last update.
     */
    @Override public void render(float delta) {
        updateScreen(delta);
        drawScreen();
    }

    public final void updateScreen(float delta) {
        if (Gdx.input.justTouched()) {
            guiCamera.unproject(touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0));

            boolean isButtonTouched = false;

            Iterator iterator = buttons.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry buttonEntry = (Map.Entry) iterator.next();

                Button button = (Button) buttonEntry.getValue();
                if (button.isTouch(touchPosition.x, touchPosition.y)) {
                    updateButtons(buttonEntry.getKey().toString());
                    isButtonTouched = true;
                }
            }

            if (!isButtonTouched) // no button was touched.
                updateIsTouch(Gdx.input.getX(), Gdx.input.getY());
        }

        update(delta);
    }

    /**
     * Last update method to be call.
     * Update screen specific items here.
     *
     * @param delta Time pass since last update.
     */
    public abstract void update(float delta);

    /**
     * Method call when a button is touch.
     *
     * @param buttonName The name of the button that was touch.
     */
    public abstract void updateButtons(String buttonName);

    /**
     * This method is call when player touch the screen and no button were touch.
     *
     * @param inputXPosition X position of the touch.
     * @param inputYPosition Y position of the touch.
     */
    public abstract void updateIsTouch(float inputXPosition, float inputYPosition);

    public final void drawScreen() {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        guiCamera.update(); // Update guiCamera position.
        spriteBatch.setProjectionMatrix(guiCamera.combined); // Set the projection matrix.

        if (isBackgroundDraw()) {
            spriteBatch.disableBlending();
            spriteBatch.begin();
            spriteBatch.draw(textures.get(TEXTURE_BACKGROUND).getTexture(), 0, 0, viewWidth, viewHeight);
            spriteBatch.end();
        }

        spriteBatch.enableBlending();
        spriteBatch.begin();
        draw();
        spriteBatch.end();
    }

    /**
     * Last method call in draw.
     * Draw screen specifics elements here.
     */
    public abstract void draw();

    /**
     * Provide a hook to draw the background (the background is draw by default).
     *
     * @return If the background is draw.
     */
    protected boolean isBackgroundDraw() {
        return true;
    }

    @Override public void resize(int width, int height) {

    }

    @Override public void show() {

    }

    @Override public void hide() {

    }

    @Override public void pause() {

    }

    @Override public void resume() {

    }

    @Override public void dispose() {

    }
}
