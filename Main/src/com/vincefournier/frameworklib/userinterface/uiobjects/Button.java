package com.vincefournier.frameworklib.userinterface.uiobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.vincefournier.frameworklib.engine.UtilitaireMath;

/**
 *
 * User: Vincent
 * Date: 30/07/13
 */
public class Button {
    public Rectangle rectangle;
    private TextureRegion textureRegion;
    private boolean isDraw = true;

    public Button(TextureRegion textureRegion, float x, float y, float width, float height) {
        this(textureRegion, new Rectangle(x, y, width, height));
    }

    public Button(TextureRegion textureRegion, Rectangle rectangle) {
        this.textureRegion = textureRegion;
        this.rectangle = rectangle;
    }

    public boolean isTouch(float x, float y) {
        if (isDraw)
            return UtilitaireMath.pointDansRectangle(rectangle, x, y);
        else
            return false; // Can't touch this ! if not draw.
    }

    public void draw(SpriteBatch spriteBatch) {
        if (isDraw)
            spriteBatch.draw(textureRegion, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    public boolean isDraw() {
        return isDraw;
    }

    public void setDraw(boolean draw) {
        isDraw = draw;
    }
}
